set title " Muestra el nombre del archivo en la ventana de la terminal
syntax on
set number " Muestra los números de las líneas
set numberwidth=1
set mouse=a " Permite la integración del mouse (seleccionar texto, mover el cursor)
set smartindent
" set rnu
" set nowrap " No dividir la línea si es muy larga
set noswapfile
set cursorline " Resalta la línea actual
set colorcolumn=120 " Muestra la columna límite a 120 caracteres
" Indentación a 2 espacios
set tabstop=4
set shiftwidth=4
set softtabstop=4
set shiftround
set expandtab " Insertar espacios en lugar de <Tab>s
set hidden " Permitir cambiar de buffers sin tener que guardarlos
set incsearch
set ignorecase " Ignorar mayúsculas al hacer una búsqueda
set smartcase " No ignorar mayúsculas si la palabra a buscar contiene mayúsculas
set clipboard=unnamedplus
set encoding=utf-8
set spelllang=en,es " Corregir palabras usando diccionarios en inglés y español
set termguicolors " Activa true colors en la terminal
" set background=dark " Fondo del tema: light o dark
